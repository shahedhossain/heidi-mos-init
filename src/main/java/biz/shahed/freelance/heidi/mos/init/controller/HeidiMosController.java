package biz.shahed.freelance.heidi.mos.init.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import biz.shahed.freelance.heidi.mos.ftl.context.FormativeContext;
import biz.shahed.freelance.heidi.mos.ftl.loader.FreeMarkerTemplateLoader;
import biz.shahed.freelance.heidi.mos.util.ResponseEntityUtil;
import biz.shahed.freelance.heidi.mos.util.service.SpringSecurityService;


@Controller
@RequestMapping("/")
public class HeidiMosController {
	
	@Autowired
	FormativeContext formativeContext;
	
	@Autowired
	private FreeMarkerTemplateLoader loader;
	
	@Autowired 
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private SpringSecurityService securityService;
	
	@RequestMapping(value="heidimos.js", method = RequestMethod.GET)
	public ResponseEntity<String> application(HttpServletRequest request, HttpServletResponse response, Locale locale) throws IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		String js = loader.loadJS(params);
		return ResponseEntityUtil.getJavaScriptResponseEntity(js);
	}
	
	@RequestMapping(value="heidimos.css", method = RequestMethod.GET)
	public ResponseEntity<String> css(HttpServletRequest request, HttpServletResponse response, Locale locale) throws IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		String css = loader.loadCSS(params);
		return ResponseEntityUtil.getCssResponseEntity(css);
	}
	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<String> html(HttpServletRequest request, HttpServletResponse response, Locale locale) throws IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", "Admin Panel");
		params.put("appName", "admin");
		
		if(securityService.isAjax(httpServletRequest)){
			return ResponseEntityUtil.getRedirectResponseEntity("account/ajaxSuccess");
		}
				
		String html = loader.loadHTMLWithDB(params);
		return ResponseEntityUtil.getHtmlResponseEntity(html, 0);
	}

}
