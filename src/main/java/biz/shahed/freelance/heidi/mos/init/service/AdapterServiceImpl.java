package biz.shahed.freelance.heidi.mos.init.service;

import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
public class AdapterServiceImpl implements AdapterService {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AdapterServiceImpl.class);

	@Autowired
	private UserAuthorityService userAuthorityService;
	
	
	@Transactional(readOnly = true)
	public Map<String, Object> getTables(){
		
		Map<String, Object> tables = new TreeMap<String, Object>();
			
//		userAuthorityService.put(tables, "M00I004001", list);
//		userAuthorityService.put(tables, "M00I000001", list);
//		userAuthorityService.put(tables, "M00I001001", list);
//		userAuthorityService.put(tables, "M00I003001", list);
//		
//		List<SenchaTreeNode> tree = userAuthorityService.getPrivilegedTreeMenu();
//		userAuthorityService.securedPut(tables, "M04I008001", tree);
		
		return tables;
	}

}
