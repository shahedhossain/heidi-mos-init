package biz.shahed.freelance.heidi.mos.init.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import biz.shahed.freelance.heidi.mos.ftl.context.FormativeContext;
import biz.shahed.freelance.heidi.mos.ftl.loader.FreeMarkerTemplateLoader;
import biz.shahed.freelance.heidi.mos.util.HeaderUtil;
import biz.shahed.freelance.heidi.mos.util.JsonUtil;
import biz.shahed.freelance.heidi.mos.util.ResponseEntityUtil;
import biz.shahed.freelance.heidi.mos.util.service.SpringSecurityService;

@Controller
@RequestMapping("/account")
public class AccountController {
	
	@Autowired
	private HttpSession session;
		
	@Autowired
	FormativeContext formativeContext;
	
	@Autowired
	private FreeMarkerTemplateLoader loader;
	
	@Autowired 
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private SpringSecurityService securityService;
		
	@Value("${heidimos.default.target.url}")
	private String defaultTarget;
	
	@Value("${heidimos.login.page}")
	private String loginPage;
	
	@Value("${heidimos.message.severity.info}")
	private String severityInfo;
	
	@Value("${heidimos.message.severity.error}")
	private String severityError;
	
	@Value("${heidimos.header.code.ok}")
	private String headerCodeOk;
	
	@Value("${heidimos.message.security.denied}")
	private String deniedMessage;
	
	@Value("${heidimos.header.code}")
	private String locationCode;
	
	@Value("${heidimos.header.key}")
	private String locationKey;
	
	@Value("${heidimos.header.location}")
	private String location;
	
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(AccountController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<String> index(){		
		String actionPath = formativeContext.getActionPath(loginPage);				
		if(securityService.isLoggedIn()){
			actionPath = formativeContext.getActionPath(defaultTarget);
		}
		return ResponseEntityUtil.getRedirectResponseEntity(actionPath);
	}
		
	@RequestMapping(value="signin", method = RequestMethod.GET)
	public ResponseEntity<String> signin(@RequestParam Map<String,String> parameters) {		
		 
		if(securityService.isAjax(httpServletRequest)){
			boolean errorFlag = false;
			if(parameters.containsKey("error")){
				if(parameters.get("error").equals("true")){
					errorFlag = true;
				}
			}
			String redirect = errorFlag ? "ajaxFailure" : "ajaxSignin";
			return ResponseEntityUtil.getRedirectResponseEntity(redirect);
		}
			
		if(securityService.isLoggedIn()){
			String actionPath = formativeContext.getActionPath(defaultTarget);
			return ResponseEntityUtil.getRedirectResponseEntity(actionPath);
		}
				
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", "Signin Panel");
		params.put("appName", "signin");
		
		String html = loader.loadHTML(params);
		return ResponseEntityUtil.getHtmlResponseEntity(html, 0);
	}
		
	@RequestMapping(value="denied", method = RequestMethod.GET)
	public ResponseEntity<String> denied(){
		
		Map<String, Object> message = new HashMap<String, Object>();
		message.put("severity", severityError);
		message.put("summary", deniedMessage);
		message.put("code", headerCodeOk);
   
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);
		json.put("message", message);
				
		if(securityService.isAjax(httpServletRequest)){
			return ResponseEntityUtil.getRedirectResponseEntity("ajaxDenied");
		}
			
		String html = JsonUtil.objectAsString(json);
		return ResponseEntityUtil.getHtmlResponseEntity(html, 0);
	}
	
	@RequestMapping(value="ajaxSignin", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> ajaxSignin(){
        String summary    = "Session time out! Signin again.";
        String targetUrl  = formativeContext.getActionPath(loginPage);
        Map<String, Object> message = new HashMap<String, Object>();
        
        message.put("severity", severityInfo);
		message.put("location", targetUrl);
		message.put("summary", summary);
		message.put("code", headerCodeOk);
   
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);
		json.put("message", message);
		
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJSON(0);
		headers.add(location, targetUrl);
		headers.add(locationKey, locationCode);
		return new ResponseEntity<Map<String,Object>>(json, headers, HttpStatus.OK);
    }
	
	@RequestMapping(value="ajaxSuccess", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> ajaxSuccess(){
        String summary    = "Authenticated successfully.";
        String targetUrl  = formativeContext.getActionPath(defaultTarget);
        Map<String, Object> message = new HashMap<String, Object>();
        
        message.put("severity", severityInfo);
		message.put("location", targetUrl);
		message.put("summary", summary);
		message.put("code", headerCodeOk);
   
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", true);
		json.put("message", message);
		
		HttpHeaders headers = HeaderUtil.getHttpHeadersForJSON(0);
		headers.add(location, targetUrl);
		headers.add(locationKey, locationCode);
		return new ResponseEntity<Map<String,Object>>(json, headers, HttpStatus.OK);
    }
	
	@RequestMapping(value="ajaxFailure", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> ajaxFailure(){
        String summary    = "";     
        Object exception  = session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        
        if (exception != null) {
        	Exception e = (Exception)exception;
        	summary = e.getMessage();
        }
        
        Map<String, Object> message = new HashMap<String, Object>();
        message.put("severity", severityError);
		message.put("summary", summary);
		message.put("code", headerCodeOk);
   
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);
		json.put("message", message);
		
		return json;
    }
		
	@RequestMapping(value="ajaxDenied", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> ajaxDenied(ModelMap model) {
		
		Map<String, Object> message = new HashMap<String, Object>();
		message.put("severity", severityError);
		message.put("summary", deniedMessage);
		message.put("code", headerCodeOk);
   
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);
		json.put("message", message);
		
		return json;
    }

}
