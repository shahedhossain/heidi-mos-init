package biz.shahed.freelance.heidi.mos.init.service;

import java.util.List;
import java.util.Map;

import biz.shahed.freelance.heidi.mos.model.security.Authority;

public interface UserAuthorityService {
	
	List<Authority> getPrivilegedAuthorities();
		
	boolean isLoggedIn();
	
	Map<String, Object> put(Map<String, Object> tables, String key, Object value);
	
	Map<String, Object> securedPut(Map<String, Object> tables, String key, Object value);

}
