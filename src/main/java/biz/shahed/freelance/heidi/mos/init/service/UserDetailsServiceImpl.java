package biz.shahed.freelance.heidi.mos.init.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import biz.shahed.freelance.heidi.mos.model.security.Authority;
import biz.shahed.freelance.heidi.mos.repo.security.AuthorityDao;
import biz.shahed.freelance.heidi.mos.repo.security.UserDao;


@Service("userDetailsService")
@Transactional(readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private AuthorityDao authorityDao;
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		biz.shahed.freelance.heidi.mos.model.security.User user = userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found");
		}
		List<Authority> authorities = authorityDao.findAllByUsername(username);
		
		sessionFactory.getCurrentSession().flush();
		return buildUser(user, authorities);
	}

	@Transactional(readOnly = true)
	private User buildUser(biz.shahed.freelance.heidi.mos.model.security.User user, List<Authority> authorities) {
		Collection<GrantedAuthority> grantedAuthorities = getGrantedAuthority(authorities);
		return (new User(user.getUsername(), user.getPassword(),
				user.isAccountActive(), user.isAccountUnexpired(),
				user.isPasswordUnexpired(), user.isAccountUnlocked(),
				grantedAuthorities));
	}

	@Transactional(readOnly = true)
	private Collection<GrantedAuthority> getGrantedAuthority(List<Authority> authorities) {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		if (authorities != null && !authorities.isEmpty()) {
			for (Authority authority : authorities) {
				grantedAuthorities.add(new SimpleGrantedAuthority(authority.getName()));
			}
		}
		return grantedAuthorities;
	}

}
