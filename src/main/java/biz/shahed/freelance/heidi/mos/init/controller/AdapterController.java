package biz.shahed.freelance.heidi.mos.init.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import biz.shahed.freelance.heidi.mos.ftl.context.FormativeContext;
import biz.shahed.freelance.heidi.mos.ftl.loader.FreeMarkerTemplateLoader;
import biz.shahed.freelance.heidi.mos.init.service.AdapterService;
import biz.shahed.freelance.heidi.mos.util.ResponseEntityUtil;

@Controller
@RequestMapping("/adapter")
public class AdapterController {
	
	@Autowired
	FormativeContext formativeContext;
	
	@Autowired
	private AdapterService adapterService;
		
	@Autowired
	private FreeMarkerTemplateLoader loader;
	
	@Autowired 
	private HttpServletRequest httpServletRequest;	

	@RequestMapping(value="javascript.js", method = RequestMethod.GET)
	public ResponseEntity<String> javascript() throws IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		String js = loader.loadJS(params);
		return ResponseEntityUtil.getJavaScriptResponseEntity(js);
	}
	
	@RequestMapping(value="offlinedb.js", method = RequestMethod.GET)
	public ResponseEntity<String> offlinedb() throws IOException {	
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tables", adapterService.getTables());		
		String js = loader.loadJS("offlinedb", params);
		return ResponseEntityUtil.getJavaScriptResponseEntity(js);
	}
	
}
