package biz.shahed.freelance.heidi.mos.init.service;

import java.util.Map;

public interface AdapterService {
	
	Map<String, Object> getTables();

}
