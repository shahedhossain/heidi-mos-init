package biz.shahed.freelance.heidi.mos.init.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import biz.shahed.freelance.heidi.mos.model.security.Authority;
import biz.shahed.freelance.heidi.mos.repo.security.AuthorityDao;
import biz.shahed.freelance.heidi.mos.util.JsonUtil;
import biz.shahed.freelance.heidi.mos.util.service.SpringSecurityService;

@Service
@Transactional(readOnly = false)
public class UserAuthorityServiceImpl implements UserAuthorityService {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(UserAuthorityServiceImpl.class);
	
	@Autowired
	private AuthorityDao authorityDao;
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	
	@Autowired
	private SpringSecurityService springSecurityService;
	
	@Transactional(readOnly = true)
	public List<Authority> getPrivilegedAuthorities(){
		List<Authority> list = new ArrayList<Authority>();
		if(springSecurityService.isLoggedIn()){
			String username = springSecurityService.getCurrentUser();
			list =  authorityDao.findAllByUsername(username);
			sessionFactory.getCurrentSession().flush();
		}
		return list;	
	}
	
	@Override	
	public boolean isLoggedIn(){
		return springSecurityService.isLoggedIn();
	}
	
	@Override
	public Map<String, Object> put(Map<String, Object> tables, String key, Object value) {
		tables.put(key, JsonUtil.objectAsString(value));
		return tables;
	}

	@Override
	public Map<String, Object> securedPut(Map<String, Object> tables, String key, Object value) {
		if (isLoggedIn()) {
			put(tables, key, value);
		}
		return tables;
	}

}
