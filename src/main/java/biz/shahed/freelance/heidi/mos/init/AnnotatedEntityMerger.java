package biz.shahed.freelance.heidi.mos.init;

import java.util.ArrayList;

public class AnnotatedEntityMerger {

	private ArrayList<String> merge = new ArrayList<String>();

	public AnnotatedEntityMerger() {

	}

	public AnnotatedEntityMerger(ArrayList<String>... all) {
		for (ArrayList<String> list : all) {
			merge.addAll(list);
		}
	}

	public ArrayList<String> merge() {
		return merge;
	}

}
