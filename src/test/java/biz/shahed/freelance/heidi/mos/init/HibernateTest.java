package biz.shahed.freelance.heidi.mos.init;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import biz.shahed.freelance.heidi.mos.model.rest.City;
import biz.shahed.freelance.heidi.mos.rest.service.CityService;

@WebAppConfiguration("webapp")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/META-INF/spring/applicationContext.xml")
public class HibernateTest {
	
	private static final Logger log = LoggerFactory.getLogger(HibernateTest.class);
	
	@Autowired
	private CityService cityService;
     

    @Before
    public void setUp() throws Exception {
        //TODO
    }

    @Test
    public void testQuery() {
    	for(City city:cityService.findAll()){
    		log.info(city.getName());
    	}
    	assertTrue(true);
    }
}