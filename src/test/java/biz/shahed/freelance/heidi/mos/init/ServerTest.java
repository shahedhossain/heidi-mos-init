package biz.shahed.freelance.heidi.mos.init;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerTest extends TestCase {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ServerTest.class);

	public ServerTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(ServerTest.class);
	}

	public void testFirst() {
		assertTrue(true);
	}
	
	public void testSecond() {
		assertTrue(true);
	}
	
	public void testMore() {
		assertTrue(true);
	}
}
