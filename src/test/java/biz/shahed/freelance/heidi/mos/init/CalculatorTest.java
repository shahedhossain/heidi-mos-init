package biz.shahed.freelance.heidi.mos.init;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import biz.shahed.freelance.heidi.mos.rest.groovy.Calculator;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration("webapp")
@ContextConfiguration(locations="classpath:/META-INF/spring/applicationContext.xml")
public class CalculatorTest {
	
	@Autowired
	@Qualifier("calculator")
	private Calculator calculator;
     

    @Before
    public void setUp() throws Exception {
        //TODO
    }

    @Test
    public void test() {
        assertEquals(calculator.add(10, 20), 30);
    }
}